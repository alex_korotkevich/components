import { FC } from "react";
type THeaderMenuAnimationProps = {
  items: string[];
};
export const HeaderMenuAnimation: FC<THeaderMenuAnimationProps> = ({
  items,
}) => {
  return (
    <ul className="HeaderMenuAnimationUl">
      {items.map((i) => (
        <li className="HeaderMenuAnimationLi">{i}</li>
      ))}
    </ul>
  );
};

// .HeaderMenuAnimationUl {
//   margin: 5%;
//   width: 450px;
//   display: flex;
//   flex-direction: row;
//   list-style: none;
//   padding: 25px 0;
//   border-radius: 25px;
//   border: 0.1px solid #099fff;
//   background-color: 18181b;
//   box-shadow: 0 4px 8px 0 #00ffff, 0 6px 20px 0 rgba(0, 0, 0, 0.19);
// }
// .HeaderMenuAnimationLi {
//   margin: auto;
//   color: white;
//   font-size: 20px;
//   cursor: pointer;
// }
// .HeaderMenuAnimationLi:after {
//   display: block;
//   content: "";
//   border-bottom: solid 1px #00ffff;
//   padding-bottom: 1%;
//   transform: scaleX(0);
//   transition: transform 300ms ease-in-out;
// }
// .HeaderMenuAnimationLi:hover:after {
//   transform: scaleX(1);
// }
