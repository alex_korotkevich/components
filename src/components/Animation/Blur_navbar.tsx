import { FC } from "react";

export const BlurNavbar: FC = () => {
  return (
    <div style={{ marginTop: "100px" }}>
      <div className="blurNavbarbody">
        <ul className="blurNavbarUl">
          <li className="blurNavbarLi">
            <a className="blurNavbarLink" href="/">
              Home
            </a>
          </li>
          <li className="blurNavbarLi">
            <a className="blurNavbarLink" href="/">
              About
            </a>
          </li>
          <li className="blurNavbarLi">
            <a className="blurNavbarLink" href="/">
              Portfolio
            </a>
          </li>
          <li className="blurNavbarLi">
            <a className="blurNavbarLink" href="/">
              Contact
            </a>
          </li>
          <li className="blurNavbarLi">
            <a className="blurNavbarLink" href="/">
              Services
            </a>
          </li>
        </ul>
      </div>
    </div>
  );
};

// .blurNavbarbody {
//   display: flex;
//   align-items: center;
//   justify-content: center;
//   height: 200px;
//   background-color: #000000;
//   font-family: sans-serif;
// }
// .blurNavbarUl {
//   margin: 0;
//   padding: 8px 0;
//   display: flex;
// }
// .blurNavbarLi {
//   list-style-type: none;
//   margin: 0 20px;
//   transition: 0.5s;
// }
// .blurNavbarLink {
//   display: block;
//   position: relative;
//   text-decoration: none;
//   font-size: 1.2em;
//   color: #e6e6ea;
//   text-transform: uppercase;
//   transition: 0.5s;
// }
// .blurNavbarUl:hover li a {
//   transform: scale(1.2);
//   opacity: 0.2;
//   filter: blur(5px);
// }
// .blurNavbarUl li a:hover {
//   transform: scale(1.8);
//   opacity: 1;
//   filter: blur(0);
// }
// .blurNavbarUl li a:before {
//   content: "";
//   position: absolute;
//   top: 0;
//   left: 0;
//   width: 100%;
//   height: 100%;
//   background: #009fb7;
//   transition: 0.5s;
//   transform: scaleX(0);
//   z-index: -1;
// }
// .blurNavbarUl li a:hover:before {
//   transition: transform 0.5s;
//   transform: scaleX(1);
// }
