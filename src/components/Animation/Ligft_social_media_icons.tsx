export const LightSocialMediaIcons = () => {
  return (
    <div className=" flex">
      <a href="/" className="iconContainer facebook">
        <i className="lightIcon fa fa-facebook"></i>
      </a>
      <a href="/" className="iconContainer linkedin">
        <i className="lightIcon fa fa-linkedin"></i>
      </a>
      <a href="/" className="iconContainer github">
        <i className="lightIcon fa fa-github"></i>
      </a>
      <a href="/" className="iconContainer twitter">
        <i className="lightIcon fa fa-twitter"></i>
      </a>
      <a href="/" className="iconContainer youtube">
        <i className="lightIcon  fa fa-youtube"></i>
      </a>
    </div>
  );
};

//  .iconContainer {
//   position: relative;
//   height: 90px;
//   width: 90px;
//   background-color: #ffffff;
//   display: flex;
//   align-items: center;
//   justify-content: center;
//   margin: 0 10px;
//   border-radius: 28px;
//   box-shadow: 0 5px 15px -5px rgb(0 0 0 / 10%);
//   cursor: pointer;
//   font-size: 36px;
//   text-decoration: none;
//   opacity: 0.99;
//   overflow: hidden;
// }
//  .iconContainer:nth-child(1) {
//   color: #3b5998;
// }
// .iconContainer:nth-child(2) {
//   color: #0077b5;
// }
//  .iconContainer:nth-child(3) {
//   color: #1b1f23;
// }
//  .iconContainer:nth-child(4) {
//   color: #5da9dd;
// }
//  .iconContainer:nth-child(5) {
//   color: #fe0108;
// }

//  .iconContainer::before {
//   content: "";
//   width: 120%;
//   height: 120%;
//   position: absolute;
//   top: 90%;
//   left: -110%;
//   -webkit-transform: rotate(45deg);
//   transform: rotate(45deg);
//   transition: all 0.35s;
//   transition-timing-function: cubic-bezier(0.31, -0.105, 0.43, 1.59);
//   z-index: 1;
// }
//  .iconContainer .lightIcon {
//   -webkit-transform: scale(0.8);
//   transform: scale(0.8);
//   transition: all 0.35s;
//   transition-timing-function: cubic-bezier(0.31, -0.105, 0.43, 1.59);
//   z-index: 2;
// }
//  .iconContainer:hover .lightIcon {
//   color: #ffffff;
//   -webkit-transform: scale(1);
//   transform: scale(1);
// }
//  .iconContainer:hover::before {
//   top: -10%;
//   left: -10%;
// }
//  .iconContainer.facebook:hover::before {
//   background-color: #3b5998;
// }
//  .iconContainer.linkedin:hover::before {
//   background-color: #0077b5;
// }
//  .iconContainer.github:hover::before {
//   background-color: #1b1f23;
// }
//  .iconContainer.twitter:hover::before {
//   background-color: #5da9dd;
// }
//  .iconContainer.youtube:hover::before {
//   background-color: #fe0108;
// }
