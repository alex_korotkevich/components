import { FC } from "react";

export const BlueInviteCard: FC = () => {
  return (
    <div className="blue_invite_card">
      <div id="card"></div>
      <h1> LONISE</h1>
      <div id="rect"></div>
      <div id="circle"></div>
    </div>
  );
};

// .blue_invite_card {
//   display: flex;
//   align-items: center;
//   justify-content: center;
//   height: 400px;
//   background-image: linear-gradient(120deg, #3664ad 0%, #78bedf 100%);
//   font-family: Verdana, Geneva, Tahoma, sans-serif;
// }
// #card {
//   width: 150px;
//   height: 300px;
//   z-index: 2;
//   background: rgba(255, 255, 255, 0.05);
//   box-shadow: 0 8px 32px 0 rgba(31, 38, 135, 0.37);
//   box-shadow: inset 1px 1px 0.12px rgba(255, 255, 255, 0.4),
//     1px 1px 3px rgba(0, 0, 0, 0.1);
//   backdrop-filter: blur(10px);
//   -webkit-backdrop-filter: blur(10px);
//   border-radius: 10px;
//   border: 1px solid rgba(255, 255, 255, 0.18);
//   animation: move1 5s ease-in-out infinite;
//   background-clip: padding-box;
// }
// h1 {
//   position: absolute;
//   z-index: 1;
//   font-size: 120px;
//   letter-spacing: 15px;
//   color: #fff;
// }
// @keyframes move1 {
//   0% {
//     transform: translateX(-200px);
//   }
//   50% {
//     transform: translateX(200px);
//   }
//   100% {
//     transform: translateX(-200px);
//   }
// }
// #rect {
//   bottom: 300px;
//   left: 70%;
//   width: 100px;
//   height: 100px;
//   background: rgba(255, 255, 255, 0.1);
//   box-shadow: 0 8px 32px 0 rgba(31, 38, 135, 0.37);
//   box-shadow: inset 1px 1px 0.12px rgba(255, 255, 255, 0.4),
//     1px 1px 3px rgba(0, 0, 0, 0.1);
//   backdrop-filter: blur(4px);
//   -webkit-backdrop-filter: blur(4px);
//   border-radius: 10px;
//   border: 1px solid rgba(255, 255, 255, 0.18);
//   position: absolute;
// }
// #circle {
//   bottom: 300px;
//   left: calc(100vw / 3);
//   width: 50px;
//   height: 50px;
//   background: rgba(255, 255, 255, 0.1);
//   box-shadow: inset 1px 1px 0.12px rgba(255, 255, 255, 0.4),
//     1px 1px 3px rgba(0, 0, 0, 0.1);
//   backdrop-filter: blur(4px);
//   -webkit-backdrop-filter: blur(4px);
//   border-radius: 50%;
//   border: 1px solid rgba(255, 255, 255, 0.18);
//   position: absolute;
// }
