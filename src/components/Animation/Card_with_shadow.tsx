export const ShadowCard = () => {
  return (
    <div className="shadowCardBody">
      <div className="shadowCard">
        <h2 className="shadowCardH1">Rotating Shadow</h2>
        <p className="shadowCardParagraph">
          Lorem ipsum dolor sit amet consectetur, adipisicing elit. Labore
          voluptates adipisci quod ut, libero autem nesciunt distinctio? Tenetur
          porro quis voluptas quaerat doloribus cum maiores numquam, vero esse
          quam temporibus.
        </p>
      </div>
    </div>
  );
};
// .shadowCardBody {
//   display: flex;
//   justify-content: center;
//   align-items: center;
//   width: 100%;
//   height: 500px;
//   overflow: hidden;
// }
// .shadowCard {
//   position: relative;
//   display: flex;
//   justify-content: center;
//   align-items: center;
//   flex-direction: column;
//   width: 300px;
//   height: 300px;
//   background: white;
//   padding: 5vmin;
//   border-radius: 2vmin;
// }
// .shadowCardH1 {
//   font-size: 25px;
//   font-weight: 600;
//   color: black;
//   text-align: center;
// }
// .shadowCardParagraph {
//   font-size: 15px;
//   color: black;
//   padding-left: 10px;
//   border-left: 2px solid navy;
//   margin: 6vmin 0;
// }

// .shadowCard::before {
//   position: absolute;
//   width: calc(80% - 2px);
//   height: calc(80% - 2px);
//   background: white;
//   border-radius: 100%;
//   filter: drop-shadow(6vmin 6vmin 6vmin rgb(242, 7, 7));
//   content: "";
//   z-index: -10;
//   animation: shadowRotate 5s linear infinite;
// }
// @keyframes shadowRotate {
//   to {
//     transform: rotateZ(360deg);
//   }
// }
