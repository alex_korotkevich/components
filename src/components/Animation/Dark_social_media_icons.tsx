import { FC } from "react";

export const DarkSocialMediaIcons: FC = () => {
  return (
    <div className="social">
      <div>
        <ul className="socialUl">
          <li className="socialLi">
            <a href="/">
              <span></span>
              <span></span>
              <span></span>
              <span></span>
              <span className="fa fa-facebook"></span>
            </a>
          </li>
          <li className="socialLi">
            <a href="/">
              <span></span>
              <span></span>
              <span></span>
              <span></span>
              <span className="fa fa-instagram"></span>
            </a>
          </li>
          <li className="socialLi">
            <a href="/">
              <span></span>
              <span></span>
              <span></span>
              <span></span>
              <span className="fa fa-twitter"></span>
            </a>
          </li>
          <li className="socialLi">
            <a href="/">
              <span></span>
              <span></span>
              <span></span>
              <span></span>
              <span className="fa fa-linkedin"></span>
            </a>
          </li>
        </ul>
      </div>
    </div>
  );
};

// .social {
//   display: flex;
//   justify-content: center;
//   align-items: center;
//   height: 350px;
//   background: #00071c;
// }
// .socialUl {
//   position: relative;
//   display: flex;
//   transform: rotate(-25deg) skew(25deg);
//   transform-style: preserve-3d;
// }
// .socialLi {
//   position: relative;
//   list-style: none;
//   width: 60px;
//   height: 60px;
//   margin: 0 20px;
// }
// .socialLi::before {
//   content: "";
//   position: absolute;
//   bottom: -10px;
//   left: -5px;
//   width: 100%;
//   height: 10px;
//   background: #2a2a2a;
//   transform: skewX(-41deg);
// }
// .socialLi::after {
//   content: "";
//   position: absolute;
//   top: 5px;
//   left: -9px;
//   width: 9px;
//   height: 100%;
//   background: #2a2a2a;
//   transform: skewY(-49deg);
// }
// .socialLi span {
//   position: absolute;
//   width: 100%;
//   height: 100%;
//   display: flex !important;
//   background: #2a2a2a;
//   justify-content: center;
//   align-items: center;
//   color: #fff;
//   font-size: 30px !important;
// }
// .socialLi:hover span {
//   z-index: 1000;
//   transition: 0.3s;
//   color: #fff;
//   box-shadow: -1px 1px 1px rgba(0, 0, 0, 0.5);
// }
// .socialLi:hover span:nth-child(5) {
//   transform: translate(40px, -40px);
//   opacity: 1;
// }
// .socialLi:hover span:nth-child(4) {
//   transform: translate(30px, -30px);
//   opacity: 0.8;
// }
// .socialLi:hover span:nth-child(3) {
//   transform: translate(20px, -20px);
//   opacity: 0.6;
// }
// .socialLi:hover span:nth-child(2) {
//   transform: translate(10px, -10px);
//   opacity: 0.4;
// }
// .socialLi:hover span:nth-child(1) {
//   transform: translate(0, 0);
//   opacity: 0.2;
// }
// .socialLi:nth-child(1):hover span {
//   background: #3ba071;
// }
// .socialLi:nth-child(2):hover span {
//   background: #2c3456;
// }
// .socialLi:nth-child(3):hover span {
//   background: #cf5c80;
// }
// .socialLi:nth-child(4):hover span {
//   background: #336b79;
// }
