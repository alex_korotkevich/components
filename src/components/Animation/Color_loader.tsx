import { FC } from "react";

export const ColorLoader: FC = () => {
  return (
    <div className="colorLoaderContainer">
      <div className="colorLoader">
        <span></span>
        <span></span>
        <span></span>
        <span></span>
      </div>
    </div>
  );
};
// .colorLoaderContainer {
//   display: grid;
//   place-items: center;
//   height: 250px;
// }
// .colorLoader {
//   position: relative;
//   width: 100px;
//   height: 100px;
//   border-radius: 50%;
//   background: linear-gradient(#14ffe9, #ffeb3b, #ff00e0);
//   animation: animate 0.5s linear infinite;
// }
// @keyframes animate {
//   0% {
//     transform: rotate(0deg);
//   }
//   100% {
//     transform: rotate(360deg);
//   }
// }
// .colorLoader span {
//   position: absolute;
//   width: 100%;
//   height: 100%;
//   border-radius: 50%;
//   background: linear-gradient(#14ffe9, #ffeb3b, #ff00e0);
// }
// .colorLoader span:nth-child(1) {
//   filter: blur(5px);
// }
// .colorLoader span:nth-child(2) {
//   filter: blur(10px);
// }
// .colorLoader span:nth-child(3) {
//   filter: blur(25px);
// }
// .colorLoader span:nth-child(4) {
//   filter: blur(50px);
// }
// .colorLoader:after {
//   content: "";
//   position: absolute;
//   top: 10px;
//   left: 10px;
//   right: 10px;
//   bottom: 10px;
//   background: #240229;
//   border-radius: 50%;
// }
