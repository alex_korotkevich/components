export const RoundedPulseButton = () => {
  return (
    <div className="flex justify-center items-center ">
      <a
        href=""
        className="roundedPulseButton
       roundedPulseButton-white roundedPulseButton-animate"
      >
        click me
      </a>
    </div>
  );
};
