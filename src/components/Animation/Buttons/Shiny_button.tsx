import { FC } from "react";
type TShinyButtonProps = {
  text: string;
};
export const ShinyButton: FC<TShinyButtonProps> = ({ text }) => {
  return (
    <div className="shinyContainer ">
      <a href="/" className="shinyLink">
        <div></div>
        <div></div>
        <div></div>
        <div></div>
        {text}
      </a>
    </div>
  );
};

// .shinyContainer {
//   display: grid;
//   height: 200px;
//   place-items: center;
//   font-family: sans-serif;
// }
// .shinyLink {
//   position: relative;
//   padding: 15px 30px;
//   /* display: inline-block; */
//   color: white;
//   /* text-transform: uppercase; */
//   letter-spacing: 4px;
//   /* text-decoration: none; */
//   font-size: 30px;
//   font-weight: 700;
//   overflow: hidden;
//   transition: 0.15s;
// }
// .shinyLink:hover {
//   color: #888;
//   background: white;
//   box-shadow: 0 0 10px white, 0 0 40px white, 0 0 80px white;
//   transition-delay: 0.9s;
//   animation: infinite;
// }
// .shinyLink div {
//   position: absolute;
//   display: block;
// }
// .shinyLink div:nth-child(1) {
//   top: 0;
//   left: -100%;
//   width: 100%;
//   height: 2px;
//   background: linear-gradient(90deg, transparent, white);
// }
// .shinyLink:hover div:nth-child(1) {
//   left: 100%;
//   transition: 0.75s;
// }
// .shinyLink div:nth-child(2) {
//   bottom: 0;
//   right: -100%;
//   width: 100%;
//   height: 2px;
//   background: linear-gradient(270deg, transparent, white);
// }
// .shinyLink:hover div:nth-child(2) {
//   right: 100%;
//   transition: 0.5s;
//   transition-delay: 0.6s;
// }
// .shinyLink div:nth-child(3) {
//   top: -100%;
//   right: 0;
//   height: 100%;
//   width: 2px;
//   background: linear-gradient(180deg, transparent, white);
// }
// .shinyLink:hover div:nth-child(3) {
//   top: 100%;
//   transition: 1s;
//   transition-delay: 0.25s;
// }
// .shinyLink div:nth-child(4) {
//   bottom: -100%;
//   left: 0;
//   height: 100%;
//   width: 2px;
//   background: linear-gradient(45deg, transparent, white);
// }
// .shinyLink:hover div:nth-child(4) {
//   bottom: 100%;
//   transition: 0.5s;
//   transition-delay: 0.75s;
// }
