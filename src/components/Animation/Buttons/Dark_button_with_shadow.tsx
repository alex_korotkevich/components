import { FC } from "react";

type TGlowingButtonProps = {
  text: string;
};
export const DarkButtonWithShadow: FC<TGlowingButtonProps> = ({ text }) => {
  return (
    <div className="glowingContainer">
      <div className="glowing">{text}</div>
    </div>
  );
};
// .glowingContainer {
//   display: flex;
//   align-items: center;
//   justify-content: center;
//   font-family: sans-serif;
// }
// .glowing {
//   display: flex;
//   justify-content: center;
//   align-items: center;
//   position: relative;
//   width: 270px;
//   height: 90px;
//   font-size: 24px;
//   text-transform: uppercase;
//   background-color: #151823;
//   animation: textColor 5s ease infinite;
// }
// .glowing::after {
//   position: absolute;
//   content: "";
//   top: 3vw;
//   z-index: -1;
//   height: 100%;
//   width: 120%;
//   transform: scale(0.75);
//   filter: blur(5vw);
//   background: linear-gradient(280deg, #0fffc1, #7e0fff);
//   background-size: 200%, 200%;
//   animation: Glow 5s ease infinite;
// }
// .glowing:hover {
//   color: white;
//   background-color: #164f59;
// }
// @keyframes Glow {
//   0% {
//     background-position: 0% 50%;
//   }
//   50% {
//     background-position: 100% 50%;
//   }
//   100% {
//     background-position: 0% 50%;
//   }
// }
// @keyframes textColor {
//   0% {
//     color: #7e0fff;
//   }
//   50% {
//     color: #0fffc1;
//   }
//   100% {
//     color: #7e0fff;
//   }
// }
