import { FC } from "react";
type TCardProps = {
  first: string;
  second: string;
};
export const Card: FC<TCardProps> = ({ first, second }) => {
  // .back {
  //   background-color: #4f346e;
  //   display: flex;
  //   flex-direction: column;
  // }
  // .words {
  //   color: #4f346e;
  //   font-size: 0;
  //   line-height: 1.5;
  // }
  // .words li {
  //   font-size: 5rem;
  //   display: inline-block;
  //   animation: move 3s ease-in-out infinite;
  //   letter-spacing: 10px;
  // }
  // @keyframes move {
  //   0% {
  //     transform: translate(-30%, 0);
  //   }
  //   50% {
  //     text-shadow: 0 25px 50px rgba(0, 0, 0, 1);
  //   }
  //   100% {
  //     transform: translate(30%, 0);
  //   }
  // }
  // .words li:nth-child(2) {
  //   animation-delay: 0.5s;
  // }
  // .words li:nth-child(3) {
  //   animation-delay: 1s;
  // }
  // .words li:nth-child(4) {
  //   animation-delay: 1.5s;
  // }
  // .words li:nth-child(5) {
  //   animation-delay: 2s;
  // }
  // .words li:nth-child(6) {
  //   animation-delay: 2.5s;
  // }
  // .words li:nth-child(7) {
  //   animation-delay: 3s;
  // }
  // .words li:nth-child(8) {
  //   animation-delay: 3.5s;
  // }
  // .words li:nth-child(9) {
  //   animation-delay: 4s;
  // }
  // .words li:nth-child(10) {
  //   animation-delay: 4.5s;
  // }
  const arr = first.toUpperCase().split("");
  const arr1 = second.toUpperCase().split("");
  console.log(arr, arr1);

  return (
    <div className="back">
      <div className="words word-1">
        <ul>
          {arr.map((i: any) => (
            <li>{i}</li>
          ))}
        </ul>
      </div>
      <div className="words word-2">
        <ul>
          {arr1.map((i: any) => (
            <li>{i}</li>
          ))}
        </ul>
      </div>
    </div>
  );
};
