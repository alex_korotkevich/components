import { FC } from "react";

export const AnimatedBalls: FC = () => {
  return (
    <>
      <div className="animatedBallsContainer">
        <div className="animatedBallsCircle"></div>
      </div>
      <div className="animatedBallsContainer">
        <div className="animatedBallsCircle"></div>
      </div>
      <div className="animatedBallsContainer">
        <div className="animatedBallsCircle"></div>
      </div>
    </>
  );
};
// .animatedBallsContainer {
//   display: flex;
//   justify-content: center;
//   align-items: center;
//   width: 100%;
//   height: 200px;
//   position: absolute;
// }
// .animatedBallsContainer:after {
//   content: "";
//   width: 20px;
//   height: 5px;
//   position: absolute;
//   background: #000;
//   border-radius: 50%;
//   bottom: 0;
//   opacity: 0.4;
//   animation: shadow 2s linear infinite;
// }
// .animatedBallsContainer:nth-child(2):after {
//   animation-delay: -0.3s;
// }
// .animatedBallsContainer:nth-child(3):after {
//   animation-delay: -0.6s;
// }
// .animatedBallsCircle {
//   width: 15px;
//   height: 15px;
//   background: #fff;
//   border-radius: 50%;
//   position: absolute;
//   animation: animate 2s linear infinite;
// }
// .animatedBallsContainer:nth-child(2) .animatedBallsCircle {
//   animation-delay: -0.3s;
// }
// .animatedBallsContainer:nth-child(3) .animatedBallsCircle {
//   animation-delay: -0.6s;
// }
// @keyframes animate {
//   0% {
//     transform-origin: 400% 50%;
//     transform: rotate(0);
//   }
//   50% {
//     transform-origin: 400% 50%;
//     transform: rotate(360deg);
//   }
//   50.1% {
//     transform-origin: -300% 50%;
//     transform: rotate(0deg);
//   }
//   100% {
//     transform-origin: -300% 50%;
//     transform: rotate(-360deg);
//   }
// }
// @keyframes shadow {
//   0% {
//   }
//   12.5% {
//     transform: translate(50px) scale(0.6);
//   }
//   25% {
//     transform: translate(110px);
//   }
//   37.5% {
//     transform: translate(50px) scale(1.4);
//   }
//   50% {
//     transform: translate(0);
//   }
//   62.5% {
//     transform: translate(-50px) scale(0.6);
//   }
//   75% {
//     transform: translate(-110px);
//   }
//   87.5% {
//     transform: translate(-50px) scale(1.4);
//   }
//   100% {
//     transform: translate(0);
//   }
// }
