import { Container, Load } from "./loader-styles";

export const Loader = () => {
  return (
    <Container>
      <Load>Loading</Load>
    </Container>
  );
};
