import styled, { keyframes } from "styled-components";

export const rotateAfter = keyframes`
0% {
  transform: rotateZ(0deg) scaleX(1) scaleY(1);
}
50% { 
  transform: rotateZ(180deg) scaleX(0.85) scaleY(0.95);
}
100% {
  transform: rotateZ(360deg) scaleX(1) scaleY(1);
}`;

export const rotateBefore = keyframes`
0% {
  transform: rotateZ(0deg) scaleX(1) scaleY(1);
}
50% {
  transform: rotateZ(-180deg) scaleX(0.95) scaleY(0.85);
}
100% {
  transform: rotateZ(-360deg) scaleX(1) scaleY(1);
}`;

export const Container = styled.div({
  position: "fixed",
  display: "flex",
  alignItems: "center",
  justifyContent: "center",
  zIndex: "2000",
  backgroundColor: "rgba(0, 0, 0, 0.3)",
  width: "100%",
  height: "100vh",
});

export const Load = styled.div`
  & {
    display: flex;
    justify-content: center;
    align-items: center;
    width: 220px;
    height: 220px;
    position: absolute;
    font-family: "Open Sans", sans-serif;
    color: #befdff;
    font-size: 26px;
  }
  &:after,
  &:before {
    content: "";
    border-radius: 100%;
    position: absolute;
    width: 100%;
    height: 100%;
  }
  &:after {
    box-shadow: inset 20px 0 0 0 rgba(255, 230, 0, 0.6),
      inset 0 20px 0 0 rgba(255, 0, 221, 0.6),
      inset -20px 0 0 0 rgba(255, 230, 0, 0.6),
      inset 0 -20px 0 0 rgba(0, 247, 255, 0.6);
    animation: ${rotateAfter} 2000ms -0.5s linear infinite;
  }
  &:before {
    box-shadow: inset 20px 0 0 0 rgba(255, 0, 0, 0.6),
      inset 0 20px 0 0 rgba(0, 4, 255, 0.6),
      inset -20px 0 0 0 rgba(0, 247, 255, 0.6),
      inset 0 -20px 0 0 rgba(255, 80, 0, 0.6);
    animation: ${rotateBefore} 2000ms -0.5s linear infinite;
  }
`;
