import axios from "axios";
import { Formik } from "formik";
import { FC, PropsWithChildren, useState } from "react";
import Button from "../Buttons/General_Button";
import { ErrorToast } from "../Tailwind/Error_Toast";

type TFormProps = PropsWithChildren<{
  initialValues: any;
  resourceName: string;
  method: string;
  postPath: string;
  navigation: string;
  formName: string;
  children: JSX.Element | JSX.Element[];
  buttonName: string;
}>;

export const Form: FC<TFormProps> = (props) => {
  const [loading, setLoading] = useState<boolean>(false);
  const [toastErrorMessage, setToastErrorMessage] = useState<string | null>(
    null
  );

  return (
    <div className="flex flex-col items-center text-gray-700">
      <Formik
        initialValues={props.initialValues}
        onSubmit={async (values, { setErrors }) => {
          setLoading(true);
          setToastErrorMessage(null);
          try {
            const data = new FormData();
            for (const key in values) {
              data.append(`${props.resourceName}[${key}]`, values[key]);
            }
            const res = await axios({
              method: props.method || "POST",
              url: props.postPath,
              data,
            });
            localStorage.setItem("notice", res.data.message);
            window.location.href = `/${props.navigation}`;
          } catch (res: any) {
            setErrors(res.response.data.errors);
            setToastErrorMessage(res.response.data.errors["base"]);
          } finally {
            setLoading(false);
          }
        }}
      >
        {({ handleSubmit }) => (
          <form
            aria-label="Form"
            className="border p-8 mt-20 sm:w-2/3 md:w-1/2 md:p-10 lg:w-1/3 lg:mt-10"
            onSubmit={handleSubmit}
          >
            <header>
              <h1 className="text-xl font-semibold text-center">
                {props.formName}
              </h1>
            </header>
            <main>
              {props.children}
              <div className="mt-3 lg:mt-7">
                {loading ? (
                  <Button variant="loading" name="Loading..." disabled={true} />
                ) : (
                  <Button
                    name={props.buttonName}
                    submit={true}
                    aria="Submit Button"
                    variant="submit"
                  />
                )}
              </div>
            </main>
            {<ErrorToast message={toastErrorMessage} />}
          </form>
        )}
      </Formik>
    </div>
  );
};
