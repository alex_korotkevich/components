import axios from "axios";
import { Form, Formik } from "formik";
import { FC, useState } from "react";
import Button from "../Buttons/General_Button";
import { Field } from "../kimia_ui/Field";

type TUploadFormProps = {
  owner: string;
};

export const UploadForm: FC<TUploadFormProps> = (props) => {
  const [loading, setLoading] = useState(false);
  const [toastErrorMessage, setToastErrorMessage] = useState<string | null>(
    null
  );

  return (
    <div>
      <Formik
        initialValues={{
          myFile: "",
        }}
        onSubmit={async (values: any, { setErrors }) => {
          setLoading(true);
          try {
            const body = new FormData();
            body.append("logo[image]", values.file);
            const res = await axios({
              method: "post",
              url: `/${props.owner}/logo`,
              data: body,
              headers: { "Content-Type": "multipart/form-data" },
            });
            localStorage.setItem("notice", res.data.message);
            window.location.href = `/${props.owner}`;
          } catch (res: any) {
            setErrors(res.response.data.errors);
            setToastErrorMessage(res.response.data.errors["base"]);
          } finally {
            setLoading(false);
          }
        }}
      >
        <Form aria-label="Upload Form">
          <Field type="file" name="image" />
          <div>
            {loading ? (
              <Button variant="loading" name="Loading..." disabled={true} />
            ) : (
              <Button
                name="Submit"
                submit={true}
                aria="Submit Button"
                variant="submit"
              />
            )}
          </div>
        </Form>
      </Formik>
    </div>
  );
};
