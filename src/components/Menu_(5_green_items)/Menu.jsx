import { useState } from "react";
// .wrapper {
//   display: flex;
//   gap: 1.5rem;
//   background-color: black;
//   padding: 10px 0px 10px 0px;
// }
// .icon {
//   position: relative;
//   display: flex;
//   width: 60px;
//   height: 60px;
//   border-radius: 30px;
//   text-decoration: none;
//   overflow: hidden;
// }
// .icon::before,
// .icon::after {
//   content: "";
//   position: absolute;
//   height: 100%;
//   width: 100%;
//   transition: all 0.7s ease;
//   border-radius: 30px;
//   border: solid #16a085 2px;
// }
// .icon i {
//   position: relative;
//   color: #ffffff;
//   font-size: 23px;
//   margin: auto;
//   transition: all 0.25s ease;
// }

// .icon:hover::before {
//   box-shadow: inset 0 0 0 60px #16a085;
// }
// .main_menu {
//   display: flex;
//   align-items: center;
//   justify-content: center;
//   width: 30%;
//   height: 100vh;
// }
// .hidden {
//   display: none;
// }
// .main_menu_buttons {
//   display: flex;
//   flex-direction: column;
//   justify-content: space-between;
//   position: relative;
//   width: 100%;
//   height: 75%;
//   background-color: black;
// }

// .main_menu_button {
//   display: flex;
//   justify-content: flex-start;
//   color: #16a085;
//   border-style: none;
//   font-size: 1.5rem; /* 24px */
//   padding: 14px 14px 14px 14px;
//   width: 100%;
// }
// .main_menu_button:hover {
//   background-color: #16a085;
//   color: white;
// }
// .main_menu_button:active {
//   background-color: #16a085;
// }
// .main_menu_button:focus {
//   background-color: #16a085;
// }
// .mr_12 {
//   margin-right: 48px;
// }
export const Menu = () => {
  const [open, setOpen] = useState(false);
  const toggle = () => {
    setOpen(!open);
  };
  return (
    <div className="wrapper">
      <a href="/" className="icon">
        <i className="fa fa-home"></i>
      </a>
      <a href="/" className="icon">
        <i className="fa fa-user"></i>
      </a>
      <button onClick={toggle}>
        <a href="#" className="icon">
          <i className={`${open ? "fa fa-times" : "fa fa-bars"}`}></i>
        </a>
      </button>
      <a href="/" className="icon">
        <i className="fa fa-sign-in"></i>
      </a>
      <a href="/" className="icon">
        <i className="fa fa-phone"></i>
      </a>
    </div>
  );
};
