import React, { FC, PropsWithChildren } from "react";
import { DropDown } from "../Dropdown (with logo in navbar)/Dropdown";

type TCurrentCandidate = {
  category: string;
  created_at: string;
  description: string;
  email: string;
  id: number;
  logo_id: number;
  name: string;
  updated_at: string;
};
type TCurrentCompany = {
  created_at: string;
  description: string;
  email: string;
  id: number;
  logo_id: number;
  name: string;
  updated_at: string;
};
type TNavbarProps = {
  current_candidate: TCurrentCandidate | null;
  current_company: TCurrentCompany | null;
  current_avatar: string;
  token: string;
};

export const Navbar: FC<TNavbarProps> = (props) => {
  return (
    <NavbarGroup className="bg-indigo-900 text-white z-10">
      <NavbarToggler />

      <NavbarLink href="/">
        <NavbarNav orientation="start">
          <div className=" ml-4 mt-0">
            <img src={""} alt="logo" width={60} />
          </div>
        </NavbarNav>
      </NavbarLink>

      <NavbarCollapse>
        {props.current_company === null && props.current_candidate === null && (
          <NavbarNav orientation="end">
            <NavbarItem>
              <NavbarLink href="/company">Company</NavbarLink>
            </NavbarItem>
            <NavbarItem>
              <NavbarLink href="/candidate">Intern</NavbarLink>
            </NavbarItem>
          </NavbarNav>
        )}

        {props.current_candidate && (
          <NavbarNav orientation="end">
            <div className="flex justify-end mr-4 lg:mr-8 md:order-2">
              <DropDown
                img={props.current_avatar}
                name={props.current_candidate.name}
                email={props.current_candidate.email}
                token={props.token}
                headerLink="/candidate"
                headerItem="Dashboard"
                firstLink="/candidates/edit"
                firstItem="Edit info"
                logoutPath="candidates"
              ></DropDown>
            </div>
          </NavbarNav>
        )}

        {props.current_company && (
          <NavbarNav orientation="end">
            <div className="flex justify-end mr-4 lg:mr-8 md:order-2">
              <DropDown
                img={props.current_avatar}
                name={props.current_company.name}
                email={props.current_company.email}
                token={props.token}
                headerLink="/company"
                headerItem="Dashboard"
                firstLink="/companies/edit"
                firstItem="Edit info"
                logoutPath="companies"
              ></DropDown>
            </div>
          </NavbarNav>
        )}
      </NavbarCollapse>
    </NavbarGroup>
  );
};

const style: any = {
  navbar: `fixed px-4 py-2 shadow top-0 w-full lg:flex lg:flex-row lg:items-center lg:justify-start lg:relative br:rounded-md`,
  brand: `py-3 text-2xl  whitespace-nowrap `,
  toggler: `block float-right text-4xl lg:hidden focus:outline-none focus:shadow`,
  item: `whitespace-pre cursor-pointer px-4 py-3 hover:text-gray-200`,
  collapse: {
    default: `border-t border-gray-400 fixed left-0 mt-2 shadow py-2 text-center lg:border-none lg:flex 
    lg:flex-grow lg:items-center lg:mt-0 lg:py-0 lg:relative lg:shadow-none`,
    open: `h-auto visible transition-all duration-500 ease-out w-full opacity-100 lg:transition-none`,
    close: `h-auto invisible w-0 transition-all duration-300 ease-in lg:opacity-100 lg:transition-none lg:visible`,
  },
  nav: {
    start: `block mb-0 mr-auto pl-0 lg:flex lg:mb-0 lg:pl-0`,
    middle: `block mb-0 ml-auto pl-0 lg:flex lg:pl-0 lg:mb-0 lg:mx-auto`,
    end: `block pl-0 mb-0 ml-auto lg:flex lg:pl-0 lg:mb-0`,
  },
};

export type TMenuContext = {
  open: boolean;
  toggle: () => void;
};

const Context = React.createContext<TMenuContext>({
  toggle: () => {},
  open: false,
});

type TNavbarGroupProps = PropsWithChildren<{ className: string }>;

type TNavbarGroup = {
  contains: (event: any) => void; // ??????????????
};

const NavbarGroup: FC<TNavbarGroupProps> = ({ children, className }) => {
  const [open, setOpen] = React.useState<boolean>(false);
  const navbarRef = React.useRef<TNavbarGroup>(null);

  const toggle = React.useCallback(() => {
    setOpen((prevState: boolean): boolean => !prevState);
  }, []);

  // close navbar on click outside when viewport is less than 1024px
  React.useEffect(() => {
    const handleOutsideClick = (event: any) => {
      // type ot event ???
      if (window.innerWidth < 1024) {
        if (!navbarRef.current?.contains(event.target)) {
          if (!open) return;
          setOpen(false);
        }
      }
    };
    window.addEventListener("click", handleOutsideClick);
    return () => window.removeEventListener("click", handleOutsideClick);
  }, [open, navbarRef]);

  return (
    <Context.Provider value={{ open, toggle }}>
      <nav ref={navbarRef as any} className={`${className} ${style.navbar}`}>
        {children}
      </nav>
    </Context.Provider>
  );
};

const useToggle = () => React.useContext(Context);

const NavbarToggler: FC = () => {
  const { toggle } = useToggle();
  return (
    <button
      type="button"
      aria-expanded="false"
      aria-label="Toggle navigation"
      className={style.toggler}
      onClick={toggle}
    >
      &#8801;
    </button>
  );
};

const NavbarCollapse: FC<PropsWithChildren> = ({ children }) => {
  const { open } = useToggle();
  return (
    <div
      style={{ backgroundColor: "inherit" }}
      className={`${style.collapse.default}
        ${open ? style.collapse.open : style.collapse.close}`}
    >
      {children}
    </div>
  );
};

type TNavbarNavProps = PropsWithChildren<{ orientation: any }>;

const NavbarNav: FC<TNavbarNavProps> = ({ children, orientation }) => (
  <ul className={style.nav[orientation]}>{children}</ul>
);

const NavbarItem: FC<PropsWithChildren> = ({ children }) => (
  <li className={style.item}>{children}</li>
);

type TNavbarLinkProps = PropsWithChildren<{ href: string }>;

export const NavbarLink: FC<TNavbarLinkProps> = ({ children, href }) => (
  <a href={href}>{children}</a>
);

export default Navbar;
