import React, { useState } from "react";

const linkClassName =
  "relative my-2 flex w-16 h-16 rounded-full no-underline before:content-[''] before:absolute before:w-full before:h-full before:transition-1 before:duration-1 before:ease before:border-2 before:rounded-full before:border-green-300 after:content-[''] after:absolute after:w-full after:h-full after:transition-1 after:duration-1 after:ease after:border-2 after:rounded-full after:border-green-300 hover:before:shadow-4";

const iClassName = "relative text-white text-2xl m-auto";

export const MenuPanelWithBurgerButton = () => {
  const [open, setOpen] = useState(false);
  const toggle = () => {
    setOpen(!open);
  };
  return (
    <>
      <div className="flex gap-6 bg-black">
        <a href="/" className={linkClassName}>
          <i className={`fa fa-home ${iClassName}`}></i>
        </a>
        <a href="/" className={linkClassName}>
          <i className={`fa fa-user ${iClassName}`}></i>
        </a>
        <button onClick={toggle}>
          <a href="/" className={linkClassName}>
            <i
              className={`${open ? "fa fa-times" : "fa fa-bars"} ${iClassName}`}
            ></i>
          </a>
        </button>
        <a href="/" className={linkClassName}>
          <i className={`fa fa-sign-in ${iClassName}`}></i>
        </a>
        <a href="/" className={linkClassName}>
          <i className={`fa fa-phone ${iClassName}`}></i>
        </a>
      </div>
      <MainMenu open={open} />
    </>
  );
};

export const MainMenu = ({ open }) => {
  return (
    <div
      className={`${
        open
          ? "flex items-center justify-center w-1/3 h-full"
          : "hidden transition-transform"
      }`}
    >
      <div className="relative flex flex-col w-full h-3/4 bg-black justify-between">
        <div>
          <MainMenuButton
            title={"About"}
            route={"/about"}
            iconName={"fa fa-info"}
          />
          <MainMenuButton
            title={"Art"}
            route={"/art"}
            iconName={"fa fa-picture-o"}
          />
          <MainMenuButton
            title={"Blog"}
            route={"/blog"}
            iconName={"fa fa-comment-o"}
          />
          <MainMenuButton
            title={"Link"}
            route={"/link"}
            iconName={"fa fa-twitter on fa-square-o"}
          />
          <MainMenuButton
            title={"Randomiser"}
            route={"/randomiser"}
            iconName={"fa fa-star"}
          />
          <MainMenuButton
            title={"Shop"}
            route={"/shop"}
            iconName={"fa fa-gift"}
          />
        </div>
        <Logout />
      </div>
    </div>
  );
};

export const Logout = () => {
  const logout = async () => {};

  return (
    <>
      <button
        className="flex justify-start text-green-300 border-none text-2xl p-3.5 w-full hover:bg-green-300
       active:bg-green-300 focus:bg-green-300 hover:text-white"
        onClick={logout}
      >
        <div className="mr-12">
          <i className="fa    fa-sign-out"></i>
        </div>
        Logout
      </button>
    </>
  );
};

export const MainMenuButton = (props) => {
  const navigate = (props) => {
    window.location.href = `/${props.route}`;
  };

  return (
    <button
      className="flex justify-start text-green-300 border-none text-2xl p-3.5 w-full hover:bg-green-300
       active:bg-green-300 focus:bg-green-300 hover:text-white"
      onClick={navigate}
    >
      <div className="mr-12">
        <i className={`${props.iconName}`}></i>
      </div>
      {props.title}
    </button>
  );
};
