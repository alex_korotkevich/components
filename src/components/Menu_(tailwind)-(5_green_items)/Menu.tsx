import { FC, useState } from "react";

export const Menu: FC = () => {
  const [open, setOpen] = useState<boolean>(false);
  const toggle = (): void => {
    setOpen(!open);
  };
  return (
    <div className="flex gap-6 bg-black">
      <a href="/" className={linkClassName}>
        <i className={`fa fa-home ${iClassName}`}></i>
      </a>
      <a href="/" className={linkClassName}>
        <i className={`fa fa-user ${iClassName}`}></i>
      </a>
      <button onClick={toggle} className={linkClassName}>
        <i
          className={`${open ? "fa fa-times" : "fa fa-bars"} ${iClassName}`}
        ></i>
      </button>
      <a href="/" className={linkClassName}>
        <i className={`fa fa-sign-in ${iClassName}`}></i>
      </a>
      <a href="/" className={linkClassName}>
        <i className={`fa fa-phone ${iClassName}`}></i>
      </a>
    </div>
  );
};

const linkClassName: string =
  "relative my-2 flex w-16 h-16 rounded-full no-underline before:content-[''] before:absolute before:w-full before:h-full before:transition-1 before:duration-1 before:ease before:border-2 before:rounded-full before:border-green-300 after:content-[''] after:absolute after:w-full after:h-full after:transition-1 after:duration-1 after:ease after:border-2 after:rounded-full after:border-green-300 hover:before:shadow-4";

const iClassName: string = "relative text-white text-2xl m-auto";
