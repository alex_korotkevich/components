import { useFormikContext } from "formik";
import { FC } from "react";
import { Field } from "./kimia_ui/Field";

type TCategoryFieldProps = {
  label: string;
  all: boolean;
  categories: string[];
};

export const CategoryField: FC<TCategoryFieldProps> = (props) => {
  const categoryOptions = props.categories.map((category: string) => (
    <option key={category}>{category}</option>
  ));
  const formik: any = useFormikContext(); // formik type???

  return (
    <Field
      label={props.label}
      type="select"
      name="category"
      value={formik.values.category}
    >
      <option value="" disabled={!props.all}>
        {(props.all && "All categories") || "Choose a category"}
      </option>
      {categoryOptions}
    </Field>
  );
};

export default CategoryField;
