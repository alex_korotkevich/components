import { Breadcrumb } from "flowbite-react";
import { FC } from "react";

type TBreadcrumbProps = {
  link: string;
  prewPageName: string;
  pageName: string;
};

export const BreadCrumb: FC<TBreadcrumbProps> = ({
  link,
  prewPageName,
  pageName,
}) => {
  return (
    <nav>
      <Breadcrumb
        role="navigation"
        aria-label="Default breadcrumb example"
        className="ml-6 mt-20 lg:ml-10 lg:mt-8 text-gray-700"
      >
        <Breadcrumb.Item href="/">Home</Breadcrumb.Item>
        <Breadcrumb.Item href={link}>{prewPageName}</Breadcrumb.Item>
        <Breadcrumb.Item>{pageName}</Breadcrumb.Item>
      </Breadcrumb>
    </nav>
  );
};
