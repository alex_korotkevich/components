import { Dropdown } from "flowbite-react";
import { FC } from "react";
import Button, { signout } from "../../Buttons/General_Button";

type TDropProps = {
  label: string;
  token: string;
  logoutPath: string;
};

export const DropdownButton: FC<TDropProps> = (props) => {
  return (
    <div className="flex justify-center">
      <Dropdown label={props.label} dismissOnClick={false}>
        <Dropdown.Item>Dashboard</Dropdown.Item>
        <Dropdown.Item>Settings</Dropdown.Item>
        <Dropdown.Item>Earnings</Dropdown.Item>
        <Dropdown.Item>
          <Button
            onClick={() => signout(props.logoutPath, props.token)}
            name="Logout"
            aria="Logout Button"
          />
        </Dropdown.Item>
      </Dropdown>
    </div>
  );
};
