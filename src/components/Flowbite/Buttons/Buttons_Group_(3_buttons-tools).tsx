import { Button } from "flowbite-react";
import { FC } from "react";
import { HiAdjustments, HiCloudDownload, HiUserCircle } from "react-icons/hi";

export const ButtonsGroup: FC = () => {
  return (
    <div className="flex flex-wrap gap-2">
      <Button.Group>
        <Button color="gray">
          <HiUserCircle className="mr-3 h-4 w-4" /> Profile
        </Button>
        <Button color="gray">
          <HiAdjustments className="mr-3 h-4 w-4" /> Settings
        </Button>
        <Button color="gray">
          <HiCloudDownload className="mr-3 h-4 w-4" /> Messages
        </Button>
      </Button.Group>
    </div>
  );
};
