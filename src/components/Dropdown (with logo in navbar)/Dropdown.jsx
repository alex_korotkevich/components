import { Avatar, Dropdown } from "flowbite-react";
import Button, { signout } from "../buttons/General_Button";

// Dropdown with Logout Button
// export to Navbar
export const DropDown = (props) => {
  return (
    <Dropdown
      aria-label="Dropdown menu"
      arrowIcon={false}
      inline={true}
      label={
        <Avatar
          aria-label="User Logo"
          alt="User settings"
          img={props.img}
          rounded={true}
        >
          {/* // props user name near avatar */}
          <span>{props.name} </span>
        </Avatar>
      }
    >
      <nav>
        <Dropdown.Header>
          {/* header text-link */}
          {props.headerItem}
          {/* user email */}
          <span>{props.email}</span>
        </Dropdown.Header>
        {/* Dropdown items */}
        <Dropdown.Item>{props.firstItem}</Dropdown.Item>
        <Dropdown.Divider />
        <Dropdown.Item>
          <Button
            onClick={() => signout(props.logoutPath, props.token)}
            name="Logout"
            aria="Logout Button"
          />
        </Dropdown.Item>
      </nav>
    </Dropdown>
  );
};
