import { Avatar, Dropdown } from "flowbite-react";
import { FC } from "react";
import Button, { signout } from "../Buttons/General_Button";

type TDropdownProps = {
  img: string;
  name: string;
  headerLink: string;
  headerItem: string;
  email: string;
  firstLink: string;
  firstItem: string;
  token: string;
  logoutPath: string;
};

// Dropdown with Logout Button
// export to Navbar
export const DropDown: FC<TDropdownProps> = (props) => {
  return (
    <Dropdown
      aria-label="Dropdown menu"
      arrowIcon={false}
      inline={true}
      label={
        <Avatar
          aria-label="User Logo"
          alt="User settings"
          img={props.img}
          rounded={true}
        >
          {/* // props user name near avatar */}
          <span>{props.name} </span>
        </Avatar>
      }
    >
      <nav>
        <Dropdown.Header>
          {/* header text-link */}
          {props.headerItem}
          {/* user email */}
          <span>{props.email}</span>
        </Dropdown.Header>
        {/* Dropdown items */}
        <Dropdown.Item>{props.firstItem}</Dropdown.Item>
        <Dropdown.Divider />
        <Dropdown.Item>
          <Button
            onClick={() => signout(props.logoutPath, props.token)}
            name="Logout"
            aria="Logout Button"
          />
        </Dropdown.Item>
      </nav>
    </Dropdown>
  );
};
