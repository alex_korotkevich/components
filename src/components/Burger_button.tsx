export const Burger = () => {
  return (
    <div className="hamburger">
      <div className=" line"></div>
      <div className="line"></div>
      <div className="line"></div>
    </div>
  );
};
// .hamburger {
//   width: 100px;
//   height: 100px;
//   padding: 20px;
//   display: flex;
//   border: solid 1px white;
//   flex-wrap: wrap;
//   justify-content: center;
//   align-items: center;
// }
// .line {
//   height: 6px;
//   width: 58px;
//   background: white;
//   margin: 5px auto;
//   transition: all 300ms;
//   transition-timing-function: cubic-bezier(0.175, 0.885, 0.32, 1.275);
// }
// .hamburger:hover,
// .hamburger:focus {
//   outline: none;
// }
// .hamburger:hover .line:nth-child(1),
// .hamburger:focus .line:nth-child(1) {
//   transform: rotate(45deg) translate(12px, 12px);
// }
// .hamburger:hover .line:nth-child(2),
// .hamburger:focus .line:nth-child(2) {
//   visibility: hidden;
// }

// .hamburger:hover .line:nth-child(3),
// .hamburger:focus .line:nth-child(3) {
//   transform: rotate(-45deg) translate(15px, -16px);
// }
