import { FC, useEffect, useState } from "react";

const CLOSE_WINDOW_TIMEOUT = 2000;

type TErrorToastProps = {
  message: string | null;
};

export const ErrorToast: FC<TErrorToastProps> = ({ message }) => {
  const [isOpen, setIsOpen] = useState<boolean>(false);

  const closeWindow = (): void => {
    setIsOpen(false);
  };
  setTimeout(closeWindow, CLOSE_WINDOW_TIMEOUT);

  useEffect(() => {
    if (message) setIsOpen(true);
  }, [message]);

  return (
    <div
      id="toast-message"
      className={`mt-12 -ml-4 sm:ml-14 md:ml-8 lg:ml-4 xl:ml-24 2xl:ml-28  absolute top-0 ${
        isOpen ? "" : "hidden"
      }`}
    >
      <div
        id="toast-danger"
        className="mt-8 flex items-center p-4 mb-4 w-full max-w-xs border border-red-500  bg-white rounded-lg "
        role="alert"
      >
        <div className="inline-flex items-center justify-center flex-shrink-0 w-8 h-8 text-red-500 bg-red-100 rounded-lg ">
          <svg
            aria-hidden="true"
            className="w-5 h-5"
            fill="currentColor"
            viewBox="0 0 20 20"
            xmlns="http://www.w3.org/2000/svg"
          >
            <path
              fillRule="evenodd"
              d="M4.293 4.293a1 1 0 011.414 0L10 8.586l4.293-4.293a1 1 0 111.414 1.414L11.414 10l4.293 4.293a1 1 0 01-1.414 1.414L10 11.414l-4.293 4.293a1 1 0 01-1.414-1.414L8.586 10 4.293 5.707a1 1 0 010-1.414z"
              clipRule="evenodd"
            ></path>
          </svg>
          <span className="sr-only">Error icon</span>
        </div>
        <div className="ml-3 text-sm font-normal text-red-600">{message}</div>
        <button
          type="button"
          className="ml-auto -mx-1.5 -my-1.5 bg-white text-gray-400 hover:text-gray-900 rounded-lg focus:ring-2 focus:ring-gray-300 p-1.5 hover:bg-gray-100 inline-flex h-8 w-8 dark:text-gray-500 dark:hover:text-white dark:bg-gray-800 dark:hover:bg-gray-700"
          data-dismiss-target="#toast-danger"
          aria-label="Close"
          onClick={closeWindow}
        >
          <span className="sr-only">Close</span>
          <svg
            aria-hidden="true"
            className="w-5 h-5"
            fill="currentColor"
            viewBox="0 0 20 20"
            xmlns="http://www.w3.org/2000/svg"
          >
            <path
              fillRule="evenodd"
              d="M4.293 4.293a1 1 0 011.414 0L10 8.586l4.293-4.293a1 1 0 111.414 1.414L11.414 10l4.293 4.293a1 1 0 01-1.414 1.414L10 11.414l-4.293 4.293a1 1 0 01-1.414-1.414L8.586 10 4.293 5.707a1 1 0 010-1.414z"
              clipRule="evenodd"
            ></path>
          </svg>
        </button>
      </div>
    </div>
  );
};
