import { FC, useState } from "react";

// .wrapper {
//     display: flex;
//     gap: 1.5rem;
//     background-color: black;
// padding: 10px 0px 10px 0px;
//   }
//   .icon {
//     position: relative;
//     display: flex;
//     width: 60px;
//     height: 60px;
//     border-radius: 30px;
//     text-decoration: none;
//     overflow: hidden;
//   }
//   .icon::before,
//   .icon::after {
//     content: "";
//     position: absolute;
//     height: 100%;
//     width: 100%;
//     transition: all 0.7s ease;
//     border-radius: 30px;
//     border: solid #16a085 2px;
//   }
//   .icon i {
//     position: relative;
//     color: #ffffff;
//     font-size: 23px;
//     margin: auto;
//     transition: all 0.25s ease;
//   }
//    .icon:hover {
//     transition-duration: 3s;
//     box-shadow: inset 0 0 0 60px #16a085;
//   }

//   .icon:hover::before {
//     box-shadow: inset 0 0 0 60px #16a085;
//   }

export const MenuPanelWithBurgerButton2: FC = () => {
  const [open, setOpen] = useState<boolean>(false);
  const toggle = () => {
    setOpen(!open);
  };
  return (
    <>
      <div className="wrapper">
        <a href="/" className="icon">
          <i className="fa fa-home"></i>
        </a>
        <a href="/" className="icon">
          <i className="fa fa-user"></i>
        </a>
        <button onClick={toggle} className="icon">
          <i className={`${open ? "fa fa-times" : "fa fa-bars"}`}></i>
        </button>
        <a href="/" className="icon">
          <i className="fa fa-sign-in"></i>
        </a>
        <a href="/" className="icon">
          <i className="fa fa-phone"></i>
        </a>
      </div>
      <MainMenu open={open} />
    </>
  );
};
type TMainMenuProps = {
  open: boolean;
};
export const MainMenu: FC<TMainMenuProps> = ({ open }) => {
  // .main_menu {
  //     display: flex;
  //     align-items: center;
  //     justify-content: center;
  //     width: 30%;
  //     height: 100vh;
  //   }
  //   .hidden {
  //     display: none;
  //   }
  //   .main_menu_buttons {
  //     display: flex;
  //     flex-direction: column;
  //     justify-content: space-between;
  //     position: relative;
  //     width: 100%;
  //     height: 75%;
  //     background-color: black;
  //   }

  //   .main_menu_button {
  //     display: flex;
  //     justify-content: flex-start;
  //     color: #16a085;
  //     border-style: none;
  //     font-size: 1.5rem;
  //     padding: 14px 14px 14px 14px;
  //     width: 100%;
  //   }
  //   .main_menu_button:hover {
  //     background-color: #16a085;
  //     color: white;
  //   }
  //   .main_menu_button:active {
  //     background-color: #16a085;
  //   }
  //   .main_menu_button:focus {
  //     background-color: #16a085;
  //   }
  //   .mr_12 {
  //     margin-right: 48px;
  //   }

  return (
    <div className={`${open ? "main_menu" : "hidden"}`}>
      <div className="main_menu_buttons">
        <div>
          <MainMenuButton
            title={"About"}
            route={"/about"}
            iconName={"fa fa-info"}
          />
          <MainMenuButton
            title={"Art"}
            route={"/art"}
            iconName={"fa fa-picture-o"}
          />
          <MainMenuButton
            title={"Blog"}
            route={"/blog"}
            iconName={"fa fa-comment-o"}
          />
          <MainMenuButton
            title={"Link"}
            route={"/link"}
            iconName={"fa fa-twitter on fa-square-o"}
          />
          <MainMenuButton
            title={"Randomiser"}
            route={"/randomiser"}
            iconName={"fa fa-star"}
          />
          <MainMenuButton
            title={"Shop"}
            route={"/shop"}
            iconName={"fa fa-gift"}
          />
        </div>
        <Logout />
      </div>
    </div>
  );
};

export const Logout: FC = () => {
  const logout = async () => {};

  return (
    <>
      <button className="main_menu_button" onClick={logout}>
        <div className="mr_12">
          <i className="fa    fa-sign-out"></i>
        </div>
        Logout
      </button>
    </>
  );
};
type TMainMenuButtonProps = {
  route: string;
  title: string;
  iconName: string;
};
export const MainMenuButton: FC<TMainMenuButtonProps> = (props) => {
  const navigate = () => {
    window.location.href = `/${props.route}`;
  };

  return (
    <button className="main_menu_button" onClick={navigate}>
      <div className="mr_12">
        <i className={`${props.iconName}`}></i>
      </div>
      {props.title}
    </button>
  );
};
