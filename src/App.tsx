// import { Loader } from "./components/Loader_Styled_Components/Loader";

import { BlueInviteCard } from "./components/Animation/Blue_invite_card";
import { BlurNavbar } from "./components/Animation/Blur_navbar";
import { DarkBlueAnimateButton } from "./components/Animation/Buttons/Dark_blue_animate_button";
import { DarkButtonWithShadow } from "./components/Animation/Buttons/Dark_button_with_shadow";
import { HoverShadowButton } from "./components/Animation/Buttons/Hover_shadow_button";
import { RoundedPulseButton } from "./components/Animation/Buttons/RoundedPulseButton";
import { ShinyButton } from "./components/Animation/Buttons/Shiny_button";
import { ShadowCard } from "./components/Animation/Card_with_shadow";
import { ColorLoader } from "./components/Animation/Color_loader";
import { DarkSocialMediaIcons } from "./components/Animation/Dark_social_media_icons";
import { HeaderMenuAnimation } from "./components/Animation/Header_menu_animation";
import { LightSocialMediaIcons } from "./components/Animation/Ligft_social_media_icons";
import { LoginForm } from "./components/Animation/Login_form";
import { Card } from "./components/Animation/Purple_floating_text";
import { RainbowDotLoader } from "./components/Animation/Rainbow_7dot_loader";
import { RotatingLoginForm } from "./components/Animation/Rotating_login_form";
import { Steps } from "./components/Animation/Steps_text";
import { Burger } from "./components/Burger_button";
import { AccordionPanel } from "./components/Flowbite/Accordion";
import { BreadCrumb } from "./components/Flowbite/Breadcrumb";
import { DropdownButton } from "./components/Flowbite/Buttons/Button_Dropdown";
import { Menu1 } from "./components/Menu_(5_green_items)/Menu";
import { Menu } from "./components/Menu_(tailwind)-(5_green_items)/Menu";
import { MenuPanelWithBurgerButton } from "./components/Menu_With_Burger(tailwind)/Menu_Panel_With_Burger";
import { MenuPanelWithBurgerButton2 } from "./components/Menu_With_Burger/Menu_Panel_With_Burger";

function App() {
  return (
    <>
      <Card first="lonise" second="internship" />
      <Steps array={["Visit", "Our", "Site"]} />
      {/* <Loader /> */}
      <BlueInviteCard />
      <BlurNavbar />
      <HeaderMenuAnimation items={["HTML", "CSS", "JAVASCRIPT", "REACT"]} />
      <MenuPanelWithBurgerButton2 />
      <MenuPanelWithBurgerButton />
      <Menu1 />
      <Menu />

      {/* <AnimatedBalls /> */}
      <ShadowCard />
      <DarkButtonWithShadow text="To homepage" />
      <ShinyButton text="Shiny Button" />
      <DarkBlueAnimateButton />
      <div style={{ display: "flex", justifyContent: "center" }}>
        <Burger />
      </div>
      <HoverShadowButton />
      <RoundedPulseButton />
      <div style={{ marginTop: "50px" }}>
        <DropdownButton label="I'm a dropdown" token="" logoutPath="" />
      </div>
      <ColorLoader />
      <RainbowDotLoader />
      <DarkSocialMediaIcons />
      <LightSocialMediaIcons />
      <LoginForm />
      <RotatingLoginForm />
      <div style={{ marginTop: "50px" }}>
        <BreadCrumb link="" prewPageName="jkwvjswv" pageName="vwvwvwv" />
      </div>
      <div style={{ marginTop: "50px" }}>
        <AccordionPanel />
      </div>

      {/* <StaticCarousel logo="" /> */}
    </>
  );
}

export default App;
