// /** @type {import('tailwindcss').Config} */
module.exports = {
  content: [
    "./src/components/**/*.{js,jsx,ts,tsx,css}",
    "node_modules/flowbite-react/**/*.{js,jsx}",
  ],
  plugins: [require("flowbite/plugin")],

  theme: {
    extend: {
      margin: {
        m1: "5px auto",
      },

      borderRadius: {
        50: "50%",
      },
      width: {
        65: "65%",
        81: "81%",
      },
      transitionProperty: {
        1: "all",
      },
      transitionDuration: {
        1: "300ms",
      },
      transitionTimingFunction: {
        "in-expo": "cubic-bezier(0.175, 0.885, 0.32, 1.275)",
        "out-expo": "cubic-bezier(0.19, 1, 0.22, 1)",
      },
      translate: {
        1: "12px, 12px",
        2: "15px, -16px",
      },
      boxShadow: {
        1: "inset 20px 0 0 0 rgba(255, 230, 0, 0.6), inset 0 20px 0 0 rgba(255, 0, 221, 0.6), inset -20px 0 0 0 rgba(255, 230, 0, 0.6), inset 0 -20px 0 0 rgba(0, 247, 255, 0.6)",
        2: " inset 20px 0 0 0 rgba(255, 0, 0, 0.6), inset 0 20px 0 0 rgba(0, 4, 255, 0.6), inset -20px 0 0 0 rgba(0, 247, 255, 0.6), inset 0 -20px 0 0 rgba(255, 80, 0, 0.6)",
        // 3: " inset 0 0 0 1px #16A085",
        4: " inset 0 0 0 60px #16A085",
      },

      letterSpacing: {
        tightest: "-.075em",
        tighter: "-.05em",
        tight: "-.025em",
        normal: "0",
        wide: ".025em",
        wider: ".05em",
        widest: ".25em",
      },
      animation: {
        rotateAfter: "rotateAfter 3s -0.5s linear infinite",
        rotateBefore: "rotateBefore 3s -0.5s linear infinite",
        // glowing: "textColor 5s ease infinite",
        // glowingAfter: "Glow 5s ease infinite",
        // if you are using the animate variant of the modal
        modal: "modal 0.5s",

        // if you are using drawer variant right
        "drawer-right": "drawer-right 0.3s",

        // if you are using drawer variant left
        "drawer-left": "drawer-left 0.3s",

        // if you are using drawer variant top
        "drawer-top": "drawer-top 0.3s",

        // if you are using drawer variant bottom
        "drawer-bottom": "drawer-bottom 0.3s",
      },
      keyframes: {
        rotateAfter: {
          "0%": { transform: "rotateZ(0deg) scaleX(1) scaleY(1)" },
          "50%": { transform: "rotateZ(180deg) scaleX(0.85) scaleY(0.95)" },
          "100%": { transform: "rotateZ(360deg) scaleX(1) scaleY(1)" },
        },
        rotateBefore: {
          "0%": { transform: "rotateZ(0deg) scaleX(1) scaleY(1)" },
          "50%": { transform: "rotateZ(-180deg) scaleX(0.95) scaleY(0.85)" },
          "100%": { transform: "rotateZ(-360deg) scaleX(1) scaleY(1)" },
        },
        // Glow: {
        //   "0%": { "background-position": "0% 50% " },
        //   "50%": { "background-position": "100% 50% " },
        //   "100%": { "background-position": "0% 50% " },
        // },
        // textColor: {
        //   "0%": { color: "#7e0fff" },
        //   "50%": { color: "#0fffc1" },
        //   "100%": { color: "#7e0fff" },
        // },
        // if you are using the animate variant of the modal
        modal: {
          "0%, 100%": { opacity: 0 },
          "100%": { opacity: 1 },
        },

        // if you are using drawer variant right
        "drawer-right": {
          "0%, 100%": { right: "-500px" },
          "100%": { right: "0" },
        },

        // if you are using drawer variant left
        "drawer-left": {
          "0%, 100%": { left: "-500px" },
          "100%": { left: "0" },
        },

        // if you are using drawer variant top
        "drawer-top": {
          "0%, 100%": { top: "-500px" },
          "100%": { top: "0" },
        },

        // if you are using drawer variant bottom
        "drawer-bottom": {
          "0%, 100%": { bottom: "-500px" },
          "100%": { bottom: "0" },
        },
      },
    },
  },
};
